package model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;


public class Ordre implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int ordreID;
	private LocalDate dato;
	private BetalingsType betalingstype;
	private ArrayList<Ordrelinie> ordrelinier;
	private int kontantRabat;
	private double procentRabat;
	
	public Ordre(int ordreID, LocalDate dato, BetalingsType betalingstype) {
		
		this.ordreID = ordreID;
		this.dato = dato;
		this.betalingstype = betalingstype;
		this.ordrelinier = new ArrayList<Ordrelinie>();
		this.kontantRabat = kontantRabat;
		this.procentRabat = procentRabat;
	}

	public ArrayList<Ordrelinie> getOrdrelinier() {
		return ordrelinier;
	}

	public int getKontantRabat() {
		return kontantRabat;
	}

	public double getProcentRabat() {
		return procentRabat;
	}

	public int getOrdreID() {
		return ordreID;
	}

	public void setOrdreID(int ordreID) {
		this.ordreID = ordreID;
	}

	public LocalDate getDato() {
		return dato;
	}

	public void setDato(LocalDate dato) {
		this.dato = dato;
	}

	public BetalingsType getBetalingstype() {
		return betalingstype;
	}

	public void setBetalingstype(BetalingsType betalingstype) {
		this.betalingstype = betalingstype;
	}

	public ArrayList<Ordrelinie> getOrderlinier() {
		return ordrelinier;
	}

	public void setOrderlinier(ArrayList<Ordrelinie> orderlinier) {
		this.ordrelinier = orderlinier;
	}

	public Ordrelinie createOrdrelinie(Pris pris, int antal) {
		Ordrelinie nyOrdrelinie = new Ordrelinie(pris, antal);
		ordrelinier.add(nyOrdrelinie);
		return nyOrdrelinie;
		}
	
	public double beregnOrdreBeløb() {

        double sum = 0.0;
        for (Ordrelinie ol : ordrelinier) {
            sum += ol.ordreLineSum();
            
            if(getKontantRabat() > 0) {
            	sum  = sum - getKontantRabat();
            }
            if(getProcentRabat() > 0) {
            	sum = sum * (1.0 - (getProcentRabat() / 100.0));
            }
        }

            return sum * (1.0 - (kontantRabat / 100.0));      

    }
	
	@Override
	public String toString() {
		return "OrdreID " + ordreID + 
				"Dato " + dato + 
				"Betalingstype " + betalingstype + 
				"Orderlinier " + ordrelinier;
	}

}
