package model;

import java.io.Serializable;
import java.util.ArrayList;

public class Prisliste {

	private String navn;
	private ArrayList<Pris> priser;

	public Prisliste(String navn) {
		this.navn = navn;
		this.priser  = new ArrayList<Pris>();
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public ArrayList<Pris> getPriser() {
		return priser;
	}

	public Pris createPris(double pris, Produkt produkt) {
		Pris nypris = new Pris(pris, produkt, this);
		produkt.addPris(nypris);
		priser.add(nypris);
		return nypris;
		}

	@Override
	public String toString() {
		return navn;
	}
	
	

}
