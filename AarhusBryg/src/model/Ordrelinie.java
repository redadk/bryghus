package model;

import java.io.Serializable;

public class Ordrelinie{
	
	private Pris pris;
	private int antal;
	
	public Ordrelinie(Pris pris, int antal) {
		super();
		this.pris = pris;
		this.antal = antal;
	}

	public Pris getPris() {
		return pris;
	}

	public void setPris(Pris pris) {
		this.pris = pris;
	}

	public int getAntal() {
		return antal;
	}

	public void setAntal(int antal) {
		this.antal = antal;
	}
	
	public double ordreLineSum() {
		double sum = pris.getPris() * antal;
		return sum;
	}

	@Override
	public String toString() {
		return pris.getProdukt() +" "+ antal + " " + ordreLineSum();
	}
	
	

}
	

