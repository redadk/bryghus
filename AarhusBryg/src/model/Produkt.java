  package model;

import java.io.Serializable;
import java.util.ArrayList;

public class Produkt{

	private String navn;
	private int lagerAntal;
	private ProduktGruppe produktGruppe;
	private ArrayList<Pris> priser;

	public Produkt(String navn, int lagerAntal, ProduktGruppe produktGruppe) {
		this.navn = navn;
		this.lagerAntal = lagerAntal;
		this.produktGruppe = produktGruppe;
		this.priser = new ArrayList<Pris>();
		
	}

	public ProduktGruppe getProduktGruppe() {
		return produktGruppe;
	}
	
	public String getNavn() {
		return navn;
	}

	public int getLagerAntal() {
		return lagerAntal;
	}
	
	public ArrayList<Pris> getPriser() {
		return priser;
	}
	
	public void addPris(Pris pris) {
		this.priser.add(pris);
	}
	
	
	
	@Override
	public String toString() {
		return navn + " pris: " + priser;
		
		
	}
	
	public String toString(String prisListeNavn) {
		String prisListePris = "";
		for (Pris pris : priser) {
		String navnPris = pris.getPrisliste().getNavn();
			if(navnPris.equals(prisListeNavn)) {
				prisListePris = pris.getPris() + "";
			}
		}
		
		return navn + " " + prisListePris + " Kr. " ;
		
	}

}

