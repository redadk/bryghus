package controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.LocalDate;


import java.util.List;

import model.BetalingsType;
import model.Ordre;
import model.Ordrelinie;
import model.Pris;
import model.Prisliste;
import model.Produkt;
import model.ProduktGruppe;
import storage.*;

public class Controller {
	
	private static Controller controller;
	private static Storage storage;
	
	public static Controller getController() {
		if (controller == null) {
			controller = new Controller();
		}
		return controller;
	}
	
	private Controller() {
		storage = Storage.getStorage();
		//loadStorage();
	}

	// -------------------Alle Opret
	// metoder---------------------------------------------------------

	public ProduktGruppe opretProduktGruppe(String navn) {
		ProduktGruppe proG = new ProduktGruppe(navn);
		storage.addProduktGruppe(proG);
		//saveStorage();
		return proG;
	}

	public Produkt opretProdukt(String navn, int lagerAntal, ProduktGruppe produktGruppe) {
		Produkt produkt = new Produkt(navn, lagerAntal, produktGruppe);
		produktGruppe.addProdukt(produkt);
		storage.addProdukt(produkt);
		//saveStorage();
		return produkt;
	}

	public Prisliste opretPrisliste(String navn) {
		Prisliste prisliste = new Prisliste(navn);
		storage.addPrisliste(prisliste);
		return prisliste;
	}

	public Pris opretPris(double pris, Produkt produkt, Prisliste prisliste) {
		Pris nypris = prisliste.createPris(pris, produkt);
		
		return nypris;
	}

	public Ordrelinie opretOrdrelinie(Ordre ordre, Pris pris, int antal) {
		Ordrelinie ordreLine = ordre.createOrdrelinie(pris, antal);
		//saveStorage();
		return ordreLine;
	}
	
	public Ordre opretOrdre(int ordreID, LocalDate dato, BetalingsType betalingstype) {
		Ordre nyOrdre = new Ordre(ordreID, dato, betalingstype);

		return nyOrdre;
	} 

	
	// --------------------------Alle Get Lister--------------------------------------------------

	public List<ProduktGruppe> getAlleProduktGrupper() {
		
		return storage.getAlleProduktGrupper();
	}

	public List<Produkt> getAlleProdukter() {
		return storage.getAlleProdukter();
	}

	public List<Prisliste> getAllePrislister() {
		return storage.getAllePrislister();
	}
	
	public List<Ordrelinie> getAlleOrdrelinier(){
		return storage.getAlleOrdrerLinier();
	}
	
	public List<Ordre> getAlleOrdrer(){
		return storage.getAlleOrdrer();
	}

	// ---------------Create Some
	// Objects-------------------------------------------------------------
	
	public void loadStorage() {

        try (FileInputStream fileIn = new FileInputStream("src/Storage.ser")) {
            try (ObjectInputStream in = new ObjectInputStream(fileIn);) {
                storage = (Storage) in.readObject(); 
                System.out.println("Storage loaded from file storage.ser.");

            } catch (ClassNotFoundException ex) {
                System.out.println("Error loading storage object.");
                throw new RuntimeException(ex);
            }

        } catch (FileNotFoundException ex) {
            storage = Storage.getStorage();

        } catch (IOException ex) {
            System.out.println("Error loading storage object.");
            throw new RuntimeException(ex);
        }
    }

	public void saveStorage() {
		try (FileOutputStream fileOut = new FileOutputStream("src/Storage.ser")) {
			try (ObjectOutputStream out = new ObjectOutputStream(fileOut)) {
				out.writeObject(storage);
				System.out.println("Storage saved in file storage.ser.");
			}
		} catch (IOException ex) {
			System.out.println("Error saving storage object.");
			throw new RuntimeException(ex);
		}
	}
	
	public void createSomeObjects() {


		this.opretProduktGruppe("Øl");
		this.opretProduktGruppe("Sodavand");
		this.opretProduktGruppe("Spiritus");

		this.opretProdukt("Mørkt", 34, storage.getAlleProduktGrupper().get(0));
		this.opretProdukt("Lyst", 12, storage.getAlleProduktGrupper().get(0));
		this.opretProdukt("Byg", 23, storage.getAlleProduktGrupper().get(0));

		this.opretProdukt("Coco Cola", 43, storage.getAlleProduktGrupper().get(1));
		this.opretProdukt("Fanta", 90, storage.getAlleProduktGrupper().get(1));
		this.opretProdukt("Miranda", 129, storage.getAlleProduktGrupper().get(1));

		this.opretProdukt("Jack Daniels", 35, storage.getAlleProduktGrupper().get(2));
		this.opretProdukt("Mor's stærke sprit", 54, storage.getAlleProduktGrupper().get(2));
		this.opretProdukt("Blæs Din Hjerne", 28, storage.getAlleProduktGrupper().get(2));

		this.opretPrisliste("Butik");

		this.opretPrisliste("Fredagsbar");

//
//		// Produkter oprettet i prisliste Butik - ØL
		this.opretPris(9.95, storage.getAlleProdukter().get(0), storage.getAllePrislister().get(0));
		this.opretPris(6.95, storage.getAlleProdukter().get(1), storage.getAllePrislister().get(0));
		this.opretPris(8.95, storage.getAlleProdukter().get(2), storage.getAllePrislister().get(0));

//		// Produkter oprettet i prisliste Butik - SODAVAND
		this.opretPris(6.95, storage.getAlleProdukter().get(3), storage.getAllePrislister().get(0));
		this.opretPris(6.95, storage.getAlleProdukter().get(4), storage.getAllePrislister().get(0));
		this.opretPris(5.95, storage.getAlleProdukter().get(5), storage.getAllePrislister().get(0));

//		// Produkter oprettet i prisliste Fredag - SPIRITUS
		this.opretPris(119.95, storage.getAlleProdukter().get(0), storage.getAllePrislister().get(1));
		this.opretPris(236.95, storage.getAlleProdukter().get(1), storage.getAllePrislister().get(1));
		this.opretPris(156.95, storage.getAlleProdukter().get(2), storage.getAllePrislister().get(1));


		

		this.opretPris(0, storage.getAlleProdukter().get(3), storage.getAllePrislister().get(1));
		this.opretPris(0, storage.getAlleProdukter().get(4), storage.getAllePrislister().get(1));
		this.opretPris(0, storage.getAlleProdukter().get(5), storage.getAllePrislister().get(1));
//		


//		// Produkter oprettet i prisliste Fredag - SODAVAND

		this.opretPris(0, storage.getAlleProdukter().get(6), storage.getAllePrislister().get(1));
		this.opretPris(6.95, storage.getAlleProdukter().get(7), storage.getAllePrislister().get(1));
		this.opretPris(5.95, storage.getAlleProdukter().get(8), storage.getAllePrislister().get(1));

//		// Produkter oprettet i prisliste Fredag - SODAVAND
		this.opretPris(0, storage.getAlleProdukter().get(3), storage.getAllePrislister().get(1));
		this.opretPris(6.95, storage.getAlleProdukter().get(4), storage.getAllePrislister().get(1));
		this.opretPris(5.95, storage.getAlleProdukter().get(5), storage.getAllePrislister().get(1));

		//Pris pr = new Pris(6.95,storage.getAlleProdukter().get(1), storage.getAllePrislister().get(1));
		
//		// Produkter oprettet i prisliste Fredag - SPIRITUS
		this.opretPris(119.95, storage.getAlleProdukter().get(6), storage.getAllePrislister().get(1));
		this.opretPris(236.95, storage.getAlleProdukter().get(7), storage.getAllePrislister().get(1));
		this.opretPris(156.95, storage.getAlleProdukter().get(8), storage.getAllePrislister().get(1));
		
		this.opretOrdre(1, LocalDate.now(), BetalingsType.KONTANT);
		this.opretOrdre(1, LocalDate.now(), BetalingsType.KONTANT);
		

		

	}
}
