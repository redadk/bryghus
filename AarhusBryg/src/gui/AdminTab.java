package gui;

import java.util.ArrayList;

import controller.Controller;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import model.Prisliste;
import model.Produkt;
import model.ProduktGruppe;

public class AdminTab extends GridPane {


	private Button opretProdukt = new Button("Opret Produkt");
	private Button opretProduktGruppe = new Button("Opret ny produktgruppe");
	private Button opretPrisListe = new Button("Opret ny prisliste");
	private ListView<ProduktGruppe> lstProduktGruppe = new ListView<ProduktGruppe>();
	private ListView<Produkt> lstProdukt = new ListView<Produkt>();
	private Button btnTilføjPris  = new Button("Tilføj Pris");
	private Controller controller;
		
	
	public AdminTab(){
	
		this.setGridLinesVisible(false);
		this.setHgap(10);
		this.setVgap(5);
		this.setPadding(new Insets(20));
		
		controller = Controller.getController();
		
		//***************Opret Produkt********************//
			
		this.add(opretProdukt, 0, 0);
		opretProdukt.setOnAction(event -> addProdukt());

		//***************Opret Produkt Gruppe********************//
		this.add(opretProduktGruppe, 1, 0);
		opretProduktGruppe.setOnAction(event -> addProduktGruppe());
	
		//***************Tiføj Pris til produkt********************//
		
		this.add(opretPrisListe, 2, 0);
		opretPrisListe.setOnAction(event -> addPrisliste() );

		//***************Produktgruppe og produkt listView********************//
		
		Label lblOpretProduktGruppe = new Label("Produktgruppe");
		this.add(lblOpretProduktGruppe, 0, 3);
		lstProduktGruppe.getItems().setAll(controller.getAlleProduktGrupper());
		this.add(lstProduktGruppe, 0, 4, 2, 5);
		lstProduktGruppe.setPrefSize(250, 150);
		
		Label lblLager = new Label("Produkter");
		this.add(lblLager, 2, 3);
		this.add(lstProdukt, 2, 4, 2, 5);
		lstProdukt.setPrefSize(250, 150);
		lstProduktGruppe
				.getSelectionModel()
				.selectedIndexProperty()
				.addListener(
						observable -> {
							ProduktGruppe selectedItem = lstProduktGruppe.getSelectionModel().getSelectedItem();
							if(selectedItem != null) {
								lstProdukt.getItems().setAll(selectedItem.getProdukter());
						}
						});
		
		lstProduktGruppe.getSelectionModel().selectFirst();
		
		this.add(btnTilføjPris, 4, 7);
		btnTilføjPris.setOnAction(event -> addPris());

	}
	
	private void addProdukt() {
		
		OpretProduktPane op = new OpretProduktPane(null);
					
		op.showAndWait();
		
		}
	
	private void addProduktGruppe() {
		
		OpretProduktGruppePane opG = new OpretProduktGruppePane(null);
		
		opG.showAndWait();
		updateControls();
		}
	
	private void addPrisliste() {
		
		OpretPrislistePane opP = new OpretPrislistePane(null);
		
		opP.showAndWait();
		updateControls();
		}
	
	private void addPris() {
		
		Produkt valgtProdukt = lstProdukt.getSelectionModel()
				.getSelectedItem();
		
		OpretPrisPane opPr = new OpretPrisPane(valgtProdukt);
		
		opPr.showAndWait();
		updateControls();
		}	
	
	public void updateControls() {
		int selected = 0;
		lstProduktGruppe.getItems().setAll(controller.getAlleProduktGrupper());
		if (lstProdukt.getSelectionModel().getSelectedItem() != null) {
			selected = lstProdukt.getSelectionModel().getSelectedIndex();
		}
				ProduktGruppe selectedItem = lstProduktGruppe.getSelectionModel().getSelectedItem();
				if(selectedItem != null) {
					lstProdukt.getItems().setAll(
							selectedItem.getProdukter());
					lstProdukt.getSelectionModel().select(selected);
				};
	}
}
