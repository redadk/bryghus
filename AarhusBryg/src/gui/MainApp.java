package gui;

import controller.Controller;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MainApp extends Application {
	
	private Controller controller = Controller.getController();
	
	public void init() {
		controller = Controller.getController();
		controller.createSomeObjects();
	}
	
//	public static void main(String[] args) {
//		Application.launch(args);
//	}
	
	@Override
	public void start(Stage stage) {
		stage.setTitle("Aarhus Bryghus Ver. 1.1");
		BorderPane pane = new BorderPane();
		this.initContent(pane);
		
		Scene scene = new Scene(pane, 1000, 500);
		stage.setScene(scene);
		stage.show();
		
	}
	
	
	private void initContent(BorderPane pane) {
		TabPane tabPane = new TabPane();
		this.initTabPane(tabPane);
		pane.setCenter(tabPane);
	}
	
//	public void stop() throws Exception {
//		controller.saveStorage();
//	}
	
	private void initTabPane(TabPane tabPane) {
		
		
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

		
		
		Tab tabButik = new Tab("Butik");
		Tab tabFredag = new Tab("Fredagsbar");
		Tab tabAdmin = new Tab("Administrator");
		
		ButikTab visButikPane = new ButikTab();
		tabButik.setContent(visButikPane);
		
		FredagsTab visFredagsPane = new FredagsTab();
		tabFredag.setContent(visFredagsPane);
		AdminTab visAdminPane = new AdminTab();
		tabAdmin.setContent(visAdminPane);
		
		tabPane.getTabs().add(tabButik);
		tabPane.getTabs().add(tabFredag);
		tabPane.getTabs().add(tabAdmin);
		
		tabButik.setOnSelectionChanged(event -> visButikPane.updateControls());
		tabAdmin.setOnSelectionChanged(event -> visAdminPane.updateControls());
		tabAdmin.setOnSelectionChanged(event -> visAdminPane.updateControls());

	}

}
