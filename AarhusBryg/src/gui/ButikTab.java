package gui;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.util.ArrayList;

import controller.Controller;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import model.BetalingsType;
import model.Ordre;
import model.Ordrelinie;
import model.Pris;
import model.Prisliste;
import model.Produkt;
import model.ProduktGruppe;

public class ButikTab extends GridPane{

	private ListView<ProduktGruppe> lstProduktGruppe = new ListView<ProduktGruppe>();
	private ListView<Produkt> lstProdukt = new ListView<Produkt>();
	private Controller controller;
	private ListView<Ordrelinie> lstSalgProdukter = new ListView<>();
	private TextField antal = new TextField();
	private Button btnTilføjSalg = new Button("Tilføj");
	private RadioButton btnKontant = new RadioButton("Kontant");
	private RadioButton btnKreditKort = new RadioButton("Kreditkort");
	private RadioButton btnMobilePay = new RadioButton("Mobilepay");
	private ToggleGroup betalingsValg = new ToggleGroup();
	private Button btnBetal = new Button("Betal");

	private Ordrelinie ordrelinie;
	//private Ordre ordre;


	
	public ButikTab() {

		controller = Controller.getController();	
		
		this.setGridLinesVisible(false);
		this.setHgap(10);
		this.setVgap(5);
		this.setPadding(new Insets(20));
		
		
		//***************ProduktGruppe********************//
		
		Label lblOpretProduktGruppe = new Label("Produktgruppe");
		this.add(lblOpretProduktGruppe, 0, 0);
		this.add(lstProduktGruppe, 0, 1, 1, 3);
		lstProduktGruppe.setPrefSize(250, 150);
		lstProduktGruppe.getItems().setAll(controller.getAlleProduktGrupper());
		
		Label lblProdukt = new Label("Produkt");
		this.add(lblProdukt, 1, 0);
		this.add(lstProdukt, 1, 1, 1, 3);
		lstProdukt.setPrefSize(250, 150);
		lstProduktGruppe
				.getSelectionModel()
				.selectedIndexProperty()
				.addListener(
						observable -> {
							ProduktGruppe selectedItem = lstProduktGruppe.getSelectionModel().getSelectedItem();
							if(selectedItem != null)
							{
								ArrayList<Produkt> butiksProdukter = new ArrayList<Produkt>();
								ArrayList<Produkt> alleProdukter = selectedItem.getProdukter();
								for (Produkt produkt : alleProdukter)								
								{
									for (Pris pris : produkt.getPriser())
									{
										if(pris.getPrisliste().getNavn().equals("Butik")) {
											butiksProdukter.add(produkt);
										}
									}
								}
								lstProdukt.getItems().setAll(butiksProdukter);
							}
						});

		lstProdukt.setCellFactory(new Callback<ListView<Produkt>, ListCell<Produkt>>() {
		    @Override
		    public ListCell<Produkt> call(ListView<Produkt> lv) {
		        return new ListCell<Produkt>() {
		            @Override
		            public void updateItem(Produkt item, boolean empty) {
		                super.updateItem(item, empty);
		                if (item == null) {
		                    setText(null);
		                } else {
		                    // assume MyDataType.getSomeProperty() returns a string
		                   setText(item.toString("Butik"));
		                }
		            }
		        };
		    }
		});
		lstProduktGruppe.getSelectionModel().selectFirst();
		Label lblSalg = new Label("Salg");
		this.add(lblSalg, 2, 0);
		
		this.add(antal, 2, 1);
		antal.setPromptText("antal");
		HBox betalingstypeValg = new HBox(8);
		this.add(btnKontant, 2, 2);
		btnKontant.setPrefWidth(80);
		btnKontant.setPrefHeight(30);
		
		this.add(btnKreditKort, 2, 2);
		btnKreditKort.setPrefWidth(100);
		btnKreditKort.setPrefHeight(30);
		
		this.add(btnMobilePay, 2, 2);
		btnMobilePay.setPrefWidth(100);
		btnMobilePay.setPrefHeight(30);
		
		betalingstypeValg.getChildren().addAll(btnKontant, btnKreditKort, btnMobilePay);
		btnKontant.setToggleGroup(betalingsValg);
		btnKreditKort.setToggleGroup(betalingsValg);
		btnMobilePay.setToggleGroup(betalingsValg);
		
		this.add(betalingstypeValg, 2, 2);
		this.add(btnTilføjSalg, 3, 1);
		btnTilføjSalg.setPrefWidth(70);
		

		this.add(lstSalgProdukter, 2, 3);
		lstSalgProdukter.setEditable(false);
		lstSalgProdukter.setPrefSize(250, 150);
		
		lstSalgProdukter.getItems().setAll(controller.getAlleOrdrelinier());
		
		btnTilføjSalg.setOnAction(event -> addOrdreLinie());
		
		this.add(btnBetal, 2, 4);
		btnBetal.setOnAction(event -> betalOdre());
	}

	public void updateControls() {
		int selected = 0;
		lstProduktGruppe.getItems().setAll(controller.getAlleProduktGrupper());
		if (lstProdukt.getSelectionModel().getSelectedItem() != null) {
			selected = lstProdukt.getSelectionModel().getSelectedIndex();
		}
				ProduktGruppe selectedItem = lstProduktGruppe.getSelectionModel().getSelectedItem();
				if(selectedItem != null) {
					lstProdukt.getItems().setAll(
							selectedItem.getProdukter());
		lstProdukt.getSelectionModel().select(selected);
				};
	}
	
	public void addOrdreLinie() {
		BetalingsType valgtBetaling;
		Produkt valgtProdukt = lstProdukt.getSelectionModel().getSelectedItem();
		Pris valgtProduktPris;
		

		for (Pris pris : valgtProdukt.getPriser())
		{
			if(pris.getPrisliste().getNavn().equals("Butik")) {
				//valgtProduktPris = this;
			}
		}
		
		if(btnKontant.isSelected()) {
			valgtBetaling = BetalingsType.KONTANT;
		}else if(btnKreditKort.isSelected()) {
			valgtBetaling = BetalingsType.KREDITKORT;
		}else {
			valgtBetaling = BetalingsType.MOBILPAY;
		}
		
		Ordre ordre = controller.opretOrdre(2, LocalDate.now(), valgtBetaling);
		
//		ordre.createOrdrelinie(lstProdukt.getSelectionModel()., antal);
//		alleOrdre.add(ordre);
//		System.out.println(alleOrdre);
//		txaSalg.getItems().setAll(controller.getAlleOrdrelinier());
//		lstProdukt.getSelectionModel().getSelectedItem()

		Pris pris1 = null;
		Produkt produkt = lstProdukt.getSelectionModel().getSelectedItem();
		
		for ( Prisliste pr : controller.getAllePrislister()) {
				if(pr.getNavn().equals("Butik")) {
				
				for (Pris pris : pr.getPriser()) {
					if(pris.getProdukt().equals(produkt)) {
						pris1 = pris;
						}
					}
				}
		}
		
		//Ordre ordre = controller.opretOrdre(1, LocalDate.now(), BetalingsType.KONTANT); 
		Ordrelinie ordreLine = controller.opretOrdrelinie(ordre, pris1, Integer.parseInt(antal.getText().trim()));
		lstSalgProdukter.getItems().add(ordreLine);

	}
	
	public void betalOdre() {
		
	}
}
