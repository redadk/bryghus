package gui;


import controller.Controller;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Pris;
import model.Prisliste;
import model.Produkt;
import storage.Storage;

public class OpretPrisPane extends Stage{

	private TextField txPris = new TextField();
	private Button btnGodkend = new Button("Godkend");
	private Button btnFortryd = new Button("Fortryd");
	
	private ComboBox<Prisliste> comboPrisliste = new ComboBox<Prisliste>();
	private Produkt produkt;
	
	private Controller controller;
	private Storage storage;
	
	public OpretPrisPane(Produkt produkt) {
		this.produkt = produkt;
		controller = Controller.getController();
		
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setMinHeight(120);
		this.setMaxHeight(220);
		this.setResizable(false);
		
		this.setTitle("Ny Pris");
		GridPane pane = new GridPane();
		this.initContent(pane);
	
		Scene scene = new Scene(pane);
		this.setScene(scene);
		
		
	}
	
	private void initContent(GridPane pane) {
		
		
		storage = Storage.getStorage();
		//storage = new Storage();
		
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);
		
		pane.add(new Label("Vælg Prisliste"), 0, 0);
		
		pane.add(comboPrisliste, 0, 1);
		comboPrisliste.getItems().addAll(controller.getAllePrislister());
		comboPrisliste.getSelectionModel().selectFirst();
		
		pane.add(new Label("Indtast pris"), 0, 2);
		pane.add(txPris, 0, 3);
		
		pane.add(btnGodkend, 0, 4);
		btnGodkend.setPrefWidth(200);
		btnGodkend.setOnAction(event -> actionOpret());
		
		btnFortryd.setPrefWidth(200);
		pane.add(btnFortryd, 0,5);
		btnFortryd.setOnAction(event -> this.hide());
		
	}

	public void actionOpret() {
		
		controller.opretPris(Double.parseDouble(txPris.getText().trim()), produkt,
		comboPrisliste.getSelectionModel().getSelectedItem());
		
		//System.out.println(controller.getAlleProdukter().get(0));
		
		this.close();
	}
	
}
