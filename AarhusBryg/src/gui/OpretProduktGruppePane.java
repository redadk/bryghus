package gui;


import controller.Controller;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.ProduktGruppe;


public class OpretProduktGruppePane extends Stage{
	
	private Button btnGodkend = new Button("Godkend");
	private Button btnFortryd = new Button("Fortryd");
	private TextField txproduktGruppe = new TextField();
	private Controller controller = Controller.getController();
	
	public OpretProduktGruppePane(ProduktGruppe produktGruppe) {

		
		this.initStyle(StageStyle.UTILITY);	
		this.initModality(Modality.APPLICATION_MODAL);
		this.setMinHeight(120);
		this.setMaxHeight(320);
		this.setResizable(false);
		
		this.setTitle("Ny Produkt Gruppe");
		GridPane pane = new GridPane();
		this.initContent(pane);
	
		Scene scene = new Scene(pane);
		this.setScene(scene);
	}
	
		
	private void initContent(GridPane pane) {
		
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);
	
		pane.add(txproduktGruppe, 0,1);
		
		btnGodkend.setPrefWidth(150);
		pane.add(btnGodkend, 0, 2);
		btnGodkend.setOnAction(event -> actionOpret());
		btnFortryd.setPrefWidth(150);
		pane.add(btnFortryd, 0,3);
		btnFortryd.setOnAction(event -> this.hide());
		
	}

	public void actionOpret() {		
		String text = txproduktGruppe.getText().trim();
		controller.opretProduktGruppe(text);
		//lvwCompanies.getItems().setAll(controller.getCompanies());
		this.close();
	}
	
}
