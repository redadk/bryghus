package gui;

import java.util.ArrayList;
import controller.Controller;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.util.Callback;
import model.Pris;
import model.Produkt;
import model.ProduktGruppe;

public class FredagsTab extends GridPane{

	private ListView<ProduktGruppe> lstProduktGruppe = new ListView<ProduktGruppe>();
	private ListView<Produkt> lstProdukt = new ListView<Produkt>();
	private Controller controller;
	
	
	
	public FredagsTab() {
		
		controller = Controller.getController();	
		
		this.setGridLinesVisible(false);
		this.setHgap(10);
		this.setVgap(5);
		this.setPadding(new Insets(20));
		
		
		//***************ProduktGruppe********************//
		
		Label lblOpretProduktGruppe = new Label("Produktgruppe");
		this.add(lblOpretProduktGruppe, 0, 0);
		this.add(lstProduktGruppe, 0, 1, 1, 2);
		lstProduktGruppe.setPrefSize(250, 150);
		lstProduktGruppe.getItems().setAll(controller.getAlleProduktGrupper());
		
		Label lblProdukt = new Label("Produkt");
		this.add(lblProdukt, 1, 0);
		this.add(lstProdukt, 1, 1, 1, 2);
		lstProdukt.setPrefSize(250, 150);
		lstProduktGruppe
				.getSelectionModel()
				.selectedIndexProperty()
				.addListener(
						observable -> {
							ProduktGruppe selectedItem = lstProduktGruppe.getSelectionModel().getSelectedItem();
							if(selectedItem != null)
							{
								ArrayList<Produkt> fredagsbarProdukter = new ArrayList<Produkt>();
								ArrayList<Produkt> alleProdukter = selectedItem.getProdukter();
								for (Produkt produkt : alleProdukter)								
								{
									for (Pris pris : produkt.getPriser())
									{
										if(pris.getPrisliste().getNavn().equals("Fredagsbar")) {
											fredagsbarProdukter.add(produkt);
										}
									}
								}
								lstProdukt.getItems().setAll(fredagsbarProdukter);
							}
						});
		
		//ListView<Produkt> listView = new ListView<>();
		lstProdukt.setCellFactory(new Callback<ListView<Produkt>, ListCell<Produkt>>() {
		    @Override
		    public ListCell<Produkt> call(ListView<Produkt> lv) {
		        return new ListCell<Produkt>() {
		            @Override
		            public void updateItem(Produkt item, boolean empty) {
		                super.updateItem(item, empty);
		                if (item == null) {
		                    setText(null);
		                } else {
		                    // assume MyDataType.getSomeProperty() returns a string
		                   setText(item.toString("Fredagsbar"));
		                }
		            }
		        };
		    }
		});
							
		
		lstProduktGruppe.getSelectionModel().selectFirst();

	}

	public void updateControls() {
		int selected = 0;
		lstProduktGruppe.getItems().setAll(controller.getAlleProduktGrupper());
		if (lstProdukt.getSelectionModel().getSelectedItem() != null) {
			selected = lstProdukt.getSelectionModel().getSelectedIndex();
		}
				ProduktGruppe selectedItem = lstProduktGruppe.getSelectionModel().getSelectedItem();
				if(selectedItem != null) {
					lstProdukt.getItems().setAll(
							selectedItem.getProdukter());
		lstProdukt.getSelectionModel().select(selected);
				};
	}
	
}
