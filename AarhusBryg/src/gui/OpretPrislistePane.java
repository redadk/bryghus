package gui;


import controller.Controller;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Prisliste;
import storage.Storage;

public class OpretPrislistePane extends Stage {

	private TextField txPrislisteNavn = new TextField();
	private Button godkendPris = new Button("Godkend");
	private Button btnFortryd = new Button("Fortryd");
	
	private Storage storage;
	private Controller controller;
	
	public OpretPrislistePane(Prisliste prisliste) {
		
		controller = Controller.getController();
		
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setMinHeight(120);
		this.setMaxHeight(220);
		this.setResizable(false);
		
		this.setTitle("Ny Prisliste");
		GridPane pane = new GridPane();
		this.initContent(pane);
	
		Scene scene = new Scene(pane);
		this.setScene(scene);
		
		
	}
	
	private void initContent(GridPane pane) {
		
		storage = Storage.getStorage();
		//storage = new Storage();
		
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);
	
		pane.add(txPrislisteNavn, 0,1);
		
		
		godkendPris.setPrefWidth(150);
		pane.add(godkendPris, 0, 2);
		godkendPris.setOnAction(event -> actionOpreta());
		
		btnFortryd.setPrefWidth(150);
		pane.add(btnFortryd, 0,3);
		btnFortryd.setOnAction(event -> this.hide());
		
	}
	
	public void actionOpreta() {		
		String text = txPrislisteNavn.getText().trim();
		controller.opretPrisliste(text);	
		this.close();
	}
}
