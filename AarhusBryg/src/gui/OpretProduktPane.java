package gui;

import controller.Controller;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Prisliste;
import model.Produkt;
import model.ProduktGruppe;

public class OpretProduktPane extends Stage {

	private ComboBox<ProduktGruppe> comboProduktGruppe = new ComboBox<ProduktGruppe>();
	private TextField txprodukt = new TextField();
	private TextField txlagerAntal = new TextField();
	private Button btnOpretProdukt = new Button("Godkend");
	private Button btnFortryd = new Button("Fortryd");

	private Controller controller = Controller.getController();

	public OpretProduktPane(Produkt produkt) {


		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setMinHeight(120);
		this.setMaxHeight(220);
		this.setResizable(true);

		this.setTitle("Opret Produkt");
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}

	private void initContent(GridPane pane) {

		pane.setPadding(new Insets(20));
		pane.setHgap(20);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		pane.add(new Label("Vælg Produkt Gruppe"), 0, 0);
		pane.add(comboProduktGruppe, 0, 1);
		comboProduktGruppe.getItems().addAll(controller.getAlleProduktGrupper());
		comboProduktGruppe.getSelectionModel().selectFirst();

		pane.add(new Label("Indtast produkt navn"), 0, 4);
		pane.add(txprodukt, 0, 5);

		pane.add(new Label("Indtast antal"), 0, 6);
		pane.add(txlagerAntal, 0, 7);

		btnOpretProdukt.setPrefWidth(200);
		pane.add(btnOpretProdukt, 0, 10);
		btnOpretProdukt.setOnAction(event -> actionOpretProdukt());
		btnFortryd.setPrefWidth(200);
		pane.add(btnFortryd, 0, 11);
		btnFortryd.setOnAction(event -> this.hide());
	}

	public void actionOpretProdukt() {
		controller.opretProdukt(txprodukt.getText().trim(), Integer.parseInt(txlagerAntal.getText().trim()),
				comboProduktGruppe.getSelectionModel().getSelectedItem());
		this.close();
	}

}
