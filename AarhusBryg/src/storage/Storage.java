package storage;

import java.util.ArrayList;
import java.util.List;
import model.Ordre;
import model.Ordrelinie;
import model.Prisliste;
import model.Produkt;
import model.ProduktGruppe;

public class Storage {

	private List<ProduktGruppe> produktGrupper;
	private List<Produkt> produkter;
	private List<Prisliste> prislister;
	private List<Ordre> ordrer;
	private List<Ordrelinie> ordrelinier;
	
	
	//---------------------------------------------------------------------

	private static Storage storage;
	
	public static Storage getStorage() {
		if (storage == null) {
			storage = new Storage();
		}
		return storage;
	}
	
	private Storage() {
		produktGrupper = new ArrayList<ProduktGruppe>();
		produkter = new ArrayList<Produkt>();
		prislister = new ArrayList<Prisliste>();
		ordrer = new ArrayList<Ordre>();
		ordrelinier = new ArrayList<Ordrelinie>();
	}

	public List<ProduktGruppe> getAlleProduktGrupper(){
		if (produktGrupper == null)
			produktGrupper = new ArrayList<ProduktGruppe>();
		return produktGrupper;
		
	}
	
	public List<Produkt> getAlleProdukter(){
		if (produkter == null)
			produkter = new ArrayList<Produkt>();
		return produkter;
		
	}
	
	public List<Prisliste> getAllePrislister(){
		if (prislister == null)
			prislister = new ArrayList<Prisliste>();
		return prislister;
		
	}
	
	public List<Ordre> getAlleOrdrer(){
		if(ordrer == null)
			ordrer = new ArrayList<Ordre>();
		return ordrer;
	}
	
	public List<Ordrelinie> getAlleOrdrerLinier(){
		if(ordrelinier == null)
			ordrelinier = new ArrayList<Ordrelinie>();
		return ordrelinier;
	}
	
	public void addProdukt(Produkt produkt) {
		if(!produkter.contains(produkt)) {
			produkter.add(produkt);
		}
			
	}
	
	public void addProduktGruppe(ProduktGruppe produktGruppe) {
		if (!produktGrupper.contains(produktGruppe)) {
			produktGrupper.add(produktGruppe);
		}
	}
	
	public void addPrisliste(Prisliste prisliste) {
		if (!prislister.contains(prisliste)) {
			prislister.add(prisliste);
		}
	}
	
}
